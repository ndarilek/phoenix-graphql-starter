FROM node:8 as builder-node
ENV NODE_ENV=production
RUN npm install -g yarn
COPY assets/package.json assets/yarn.lock assets/
WORKDIR /assets
RUN env NODE_ENV=dev yarn install
COPY /assets/ .
RUN yarn build

FROM elixir:1.5.1-alpine as builder-elixir
ENV MIX_ENV=prod
WORKDIR /app
RUN apk update
RUN apk add libsodium-dev git make gcc musl-dev
RUN rm -rf /var/cache/apk/*
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir config
COPY config/* config/
COPY mix.exs mix.lock ./
RUN mix do deps.get, deps.compile
COPY --from=builder-node /priv/static ./priv/static
RUN mix phx.digest
COPY ./ ./
RUN mix release --env=prod --verbose

FROM alpine
ENV PORT=4000 MIX_ENV=prod REPLACE_OS_VARS=true SHELL=/bin/sh
EXPOSE $PORT
WORKDIR /app
RUN apk update
RUN apk add bash libsodium openssl ca-certificates
RUN rm -rf /var/cache/apk/*
COPY --from=builder-elixir /app/_build/prod/rel/app/ ./
RUN find . -name app.tar.gz -delete
ENTRYPOINT ["./bin/app"]
CMD ["foreground"]
