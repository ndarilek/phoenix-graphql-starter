defmodule App.Repo.Migrations.CreatePasswords do
  use Ecto.Migration

  def change do

    create table(:passwords, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :value, :string, null: false
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id), null: false

      timestamps()
    end

    create index(:passwords, [:user_id])
  end
end
