defmodule App.Accounts.Password do
  alias App.Accounts.User
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "passwords" do
    field :value, Comeonin.Ecto.Password
    belongs_to :user, User

    timestamps()
  end

end
