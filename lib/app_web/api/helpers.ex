defmodule AppWeb.API.Helpers do

  def to_graphql_errors(changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(fn( {msg, opts}) ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
    |> Map.to_list
    |> Enum.map(fn(v) ->
      field = elem(v, 0) |> Atom.to_string
      elem(v, 1)
      |> Enum.map(&("#{field} #{&1}"))
      
    end)
    |> List.flatten
  end

end
